TARGET = tf-idf

all: $(TARGET)

$(TARGET): $(TARGET).cpp
 	g++ -g -Wall -std=c++11 -o $(TARGET) $(TARGET).cpp

clean:
	$(RM) $(TARGET)