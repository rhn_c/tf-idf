#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <math.h>


void parseDocument(std::vector<std::string> stopWords, std::string &line, std::string &id, std::map<std::string, int> &wordCount);
bool isNotValidChar(char c);
bool isAlphaChar(char c);


int main()
{
	bool 							outputToFile = true;				//set to true to output the results to "results.txt"
	std::ofstream 					outputFile;							//for writing to output file
	std::ifstream 					file("./tf_idf_dataset.tsv");		/*The input file (assumes the corpus file is in the same directory as the compiled program,
																			otherwise alter the path accordingly)*/					
	std::string   					line;								//Container for each document within the corpus
	int 							docCount;							//The number of documents in the corpus
	std::string 					docID;								//The Document ID
	std::vector<std::string> 		docIDList;							//Storage for accumulating all the document IDs
	std::map<std::string, int> 		wordCount;							//A map of unique words in a single document to their respective frequency count
	std::vector<int> 				maxFreqList;						//Track the most frequently occuring word in each document for each document in the corpus
	std::vector<std::map<std::string, int> > wordCountList;				//The container of word counts for each document
	std::map<std::string, int> 		wordCountGlobal;					//The overall count of word frequencies across all documents
	std::map<std::string, double> 	idfScores;							//The inverse document frequency scores across all documents
	std::map<std::string, double> 	tf_idfScores;						//The final tf-idf scores for a single document
	double 							topScores[3];						//Storage for the top 3 tf-idf scores in the document
	std::string 					topWords[3];						//Storage for the words associated with the top 3 tf-idf scores in the document

	//The list of stop words to ignore ( source http://www.ranks.nl/stopwords )
	const char* stopWordInit[] = { "a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "i", "ie", "if", "i'm", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "we're", "were", "weren't", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "wouldn't", "yet", "you", "your", "you're", "yours", "yourself", "yourselves", "the" };
	
	std::vector<std::string> stopWords(stopWordInit, stopWordInit + sizeof(stopWordInit) / sizeof(stopWordInit[0]));

	std::sort(stopWords.begin(), stopWords.end()); //sort the stopWords to improve time taken for later comparisons

	if (outputToFile){
		outputFile.open("results.txt");
	}

	//Parse each document in the input file (critical error if no corpus file found at the given path) to generate word frequency counts
	//worst case running time occurs when every word in every document is unique, let n be the number of documents and w be the number of words
	// => O(n*w*log(w))
	if (file.is_open()){
		while (file.good()){
			//Split the file by new lines
			if (std::getline(file, line, '\n')){
				//Clear the "per document" containers
				docID.clear();
				wordCount.clear();
				//Update the ID and word count specific to the current document
				parseDocument(stopWords, line, docID, wordCount);
				//Update the containers that track information for all documents in the corpus
				docIDList.push_back(docID);
				wordCountList.push_back(wordCount);
				
				//Find the count of the highest frequency word in the document, linear complexity to number of unique words in the document
				int maxCount = 0;
				for (std::map<std::string, int>::iterator it = wordCount.begin(); it != wordCount.end(); it++){
					if (maxCount < it->second){
						maxCount = it->second;
					}
				}
				maxFreqList.push_back(maxCount);

				/*Update the corpus-wide word frequency counts 
				Time complexity = (linear cost to number of unique words in document) * (cost of searching map: logarithmic worst case for c++ map implementation)
				*/
				for (std::map<std::string, int>::iterator it = wordCount.begin(); it != wordCount.end(); it++){
					wordCountGlobal[it->first] += it->second;
					++idfScores[it->first]; //update the count of how many documents contain a given word
				}

			}//Document loop
		}//Corpus file loop
	}
	else{
		std::cout << "Error: could not open corpus file" << std::endl;
		getchar(); //pause to display input
		exit(1);
	}



	//Once all the documents in the corpus have been processed and the frequency counts generated, start computing the tf-idf
	docCount = docIDList.size();
	//Compute the full idf scores for each word in the corpus using the inital counts taken previously, complexity is linear to the number of unique words in the corpus
	for (std::map<std::string, double>::iterator it = idfScores.begin(); it != idfScores.end(); it++){
		idfScores[it->first] = log10((1.0*docCount) / (1.0 + it->second));
	}

	//For each document: compute if-idf scores and map it to the word
	for (int i = 0; i < docCount; i++){

		std::map<std::string, int> & docWordCount = wordCountList.at(i);
		std::string & id = docIDList.at(i);
		tf_idfScores.clear(); //clear the results populated from the last document

		for (std::map<std::string, int>::iterator it = docWordCount.begin(); it != docWordCount.end(); it++){
			double tf = 0.5 + ((0.5*it->second) / (1.0*maxFreqList.at(i)));  //use the augmented frequency definition of term frequency
			tf_idfScores[it->first] = tf*idfScores[it->first];
		}


		//Zero the max scores/associated words
		for (int i = 0; i < 3; i++){
			topScores[i] = 0.0;
			topWords[i].clear();
		}
		/*Find the top 3 if-idf scores - Single pass, time complexity is linear to (unique words per doc) * (number of doc)
		(note this block is a bit ugly, but its considerably faster to manually keep a sorted top 3 and do no sorting on the rest of the list in a single linear pass
		rather than sorting the full mapping and taking the top 3 results)
		*/
		for (std::map<std::string, double>::iterator it = tf_idfScores.begin(); it != tf_idfScores.end(); it++){
			if (it->second>topScores[0]){
				topScores[2] = topScores[1];
				topWords[2] = topWords[1];
				topScores[1] = topScores[0];
				topWords[1] = topWords[0];
				topScores[0] = it->second;
				topWords[0] = it->first;
			}
			else if (it->second > topScores[1]){
				topScores[2] = topScores[1];
				topWords[2] = topWords[1];
				topScores[1] = it->second;
				topWords[1] = it->first;
			}
			else if (it->second > topScores[2]){
				topScores[2] = it->second;
				topWords[2] = it->first;
			}
			
		}

		//output the result
		std::cout << id <<
			'\t' << topWords[0] << '\t' << topScores[0] <<
			'\t' << topWords[1] << '\t' << topScores[1] <<
			'\t' << topWords[2] << '\t' << topScores[2] << std::endl;

		
		if (outputToFile && outputFile.good()){
			outputFile << id <<
				'\t' << topWords[0] << '\t' << topScores[0] <<
				'\t' << topWords[1] << '\t' << topScores[1] <<
				'\t' << topWords[2] << '\t' << topScores[2] << std::endl;
		}
	}


	getchar(); //pause to display input
	exit(0);
}

/**
Takes a string containing the document from the given corpus and a list of stop
words to ignore and returns the document ID and word frequency map.
@param stopWords - The list of words that should be ignored in the frequency counts (e.g "a", "the", "any", "about", etc)
@param line - The document's full text
@param id   - return storage for the document's id, assumed to be allocated before calling
@param wordCount - return storage for the map of unique words to their frequency count, assumed to be allocated before calling
**/
void parseDocument(std::vector<std::string> stopWords, std::string &line, std::string &id, std::map<std::string, int> &wordCount){

	std::string rawText;	//Container for the raw text of the Doument
	std::string word;		//Container to be used for each string in the text

	//Find the position of the tab character that seperates the Document ID from the Document Contents
	int tabPos = line.find('\t');
	
	//Ensure the line did contain a tab character
	if (tabPos != std::string::npos){
		id = line.substr(0, tabPos); 
		rawText = line.substr(tabPos, line.length()); 

		/*Strip the text of any non-alphanumeric character except for spaces and apostrophes. The function chosen as the predicate for remove_if returns true if 
		the characters should be removed (e.g an invalid character could be punctuation). remove_if functions by replacing an invalid character by the next valid
		character, preserving the order of valid elements, then returning an interator at the location where the invalid characters begin (i.e the "past the end" element). 
		Erase removes every char after the "past the end" iterator.
		It is single pass and linear in complexity to the number of characters in the raw text.*/
		rawText.erase(std::remove_if(rawText.begin(), rawText.end(), isNotValidChar), rawText.end());

		//Inplace convesion of all characters to lower case, necessary for hashing - linear in complexity to the number of characters in the raw text
		std::transform(rawText.begin(), rawText.end(), rawText.begin(), ::tolower);

		//Iterate through each word in the processed text and store their frequency in the word map
		std::stringstream ss(rawText);
		while (ss >> word){
			bool skip = false;
			//remove leading or trailling apostrophes
			if (word.length() > 0 && word.at(0) == '\''){
				word = word.substr(1, word.length());
			}

			if (word.length() > 0 && word.at(word.length() - 1) == '\''){
				word = word.substr(0, word.length()-1);
			}

			//Remove possessiveness from nouns (i.e Langston's should be treated as Langston)
			if (word.length() > 2 && (word.substr(word.length() - 2).compare ("'s") == 0)){
				word = word.substr(0,word.length() - 2);
			}

			/*Ignore stop words (i.e "a", "he's", "while"), worst case complexity is linear to the (size of stop word file) * (size of raw text),
			but the stop word file is sorted so it is log(size of stop word file) * (size of raw text)*/
			if (std::binary_search(stopWords.begin(), stopWords.end(), word)){
				skip = true;
			}

			//skip words that contain no letters but fall in the range otherwise (weird edge cases like " ''''''''''''''''' ")
			if (std::count_if(word.begin(), word.end(), isAlphaChar) == 0){
				skip = true;
			}
			//update the document's word frequency map
			if (!skip){
				++wordCount[word];
			}
		}

	}
}

/**
Predicate function that returns true if the given character is a valid character, assumes that the file was encoded as ASCII.
This function is intended to be used to remove unwanted punctuation and symbols
@param c - the character whose validity is assessed by the function
@return - true if the input character is valid, otherwise false
**/
bool isNotValidChar(char c){
	return !(32 == c || 39 == c ||      // Character is a space or apostrophe
			(c >= 'A' && c <= 'Z') ||  // Character is lower or uppercase (note case transformation hasn't occured yet)
			(c >= 'a' && c <= 'z' ));  
}

/**
Predicate function that returns true if the given character is a an alphabetical character, assumes that the file was encoded as ASCII.
@param c - the character to test whether or not it is alphabetical
@return - true if the input character is in the alphabetical range, otherwise false
**/
bool isAlphaChar(char c){
	return ((c >= 'A' && c <= 'Z') ||(c >= 'a' && c <= 'z'));
}